import 'package:flutter/material.dart';
import './textoutput.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TUGAS UTS MOBILE APP',
      home: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          leading: new Icon(Icons.home),
          title: Text('TUGAS PEMROGRAMAN MOBILE (RETNO NC'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: TextOutput(),
      ),
    );
  }
}